# Testfallmatrix

| Use Case              | Erwartung                                                                                                                                                                                                                                                                                       | Realität                                                  | Status |
| --------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | --------------------------------------------------------- | ------ |
| Zutaten auswählen     | Es ist möglich Zutaten auszuwählen und aber auch wieder zu entfernen                                                                                                                                                                                                                            | OK                                                        | OK     |
| Getränkt auswählen    | Es ist möglich ein oder kein Getränk in einer Dropdown Auswahl auszuwählen                                                                                                                                                                                                                      | OK                                                        | OK     |
| Datum/Uhrzeit         | Es ist möglich die Lieferzeit auf eine bestimmte Uhrzeit zu setzen                                                                                                                                                                                                                              | OK                                                        | OK     |
| Newsletter abonnieren | Es ist möglich den Newsletter zu abonnieren oder nicht zu abonnieren. Es kann nur eine Auswahl getroffen werden                                                                                                                                                                                 | OK                                                        | OK     |
| Kommentar erfassen    | Es gibt ein Textfeld, in welchem ein beliebig langer Kommentar erfasst werden kann.                                                                                                                                                                                                             | Im Mockup fehlt das Kommentarfeld                         | NOK    |
| Abbruch               | Sobald auf Abbruch gedruckt wird, wird der ganze Bestellprozess beendet und alles Auswahlen verschwinden                                                                                                                                                                                        | OK                                                        | OK     |
| Absenden 1            | Durch Drücken Ddes Fertig-Buttons gelangt man auf die Bestellübersicht.                                                                                                                                                                                                                         | OK                                                        | OK     |
| Bestellübersicht      | Auflistung von alles ausgewählten Produkten auf der Pizza. Zudem ist das Getränk auch aufgelistet. Der Kommentar und die Lieferzeit werden auch nochmals aufgeführt. Auf der linken Seite gibt es die Möglichkeit Newsletter zu abonnieren, Emailadresse anzugeben und den Fahrer/in zu wählen. | OK (Der Kommentar fehlt, da auch kein Feld vorhanden ist) | NOK    |
| Zurück                | Man gelangt wieder auf Seite 1.                                                                                                                                                                                                                                                                 | OK                                                        | OK     |
| Absenden              | Bestellung wird endgültig abgesendet.                                                                                                                                                                                                                                                           | OK                                                        | OK     |
| Abbrechen             | Bestellung wird endgültig abgebrochen.                                                                                                                                                                                                                                                          | OK                                                        | OK     |

\
&nbsp;
\
&nbsp;
\
&nbsp;

# Usability Testing
Im folgenden werden wir usere Vorgehensweise dokumentieren, wie wir unser Frontend einem Usability Test unterzogen haben. <br/>
Sobald wir Testpersonen gefunden haben werden wir die Tests mit den gefundenen Personen durchführen. Dieser Test soll uns zeigen, ob unser Frontend gut aufgebaut ist.<br/>
Falls der/die Tester/in Schwierigkeiten beim Bedienen der Bestellseite haben sollte, wissen wir, an welcher stelle wir unser Design noch verbessern müssen.
\
&nbsp;\
&nbsp;

## Usability Testing

Der/Die Tester hat eine "Aufgabenstellung" erhalten, welche im unteren Kapitel notiert wurde. Das Ziel war, dass mit Hilfe des Mokups und Wireframes die Testende Person erklären kann, was gedrückt werden müsste, wenn er/sie auf der richtigen Seite useres Bestellservices gewesen wäre. Der/Die Beobachter/in notiert sich alles, was die testende Person sagt und wo eventuell Schwierigkeiten aufgetreten sind.

### Unser Vorgehen beim Testing

1. Testkonzept erstellen (Aufgabenstellung)
2. Testpersonen rekrutieren 
3. Testvorbereitungen
4. Prototyptestfähig machen (Kein Prototyp, sondern Walk through mit Wireframe)
5. DurchführungderTests
6. Auswertung der Testergebnisse (Tabelle)
7. Anpassung des Konzeptes oder Erarbeitung von Verbesserungsvorschlägen


### Aufgabenstellung 1

Du bestellst für Dich eine Pizza mit einem Getränk. Die Pizza ist mit Salami, Schinken und Mozarella. Das Getränk soll CocaCola sein.
Die Liegerung soll um 17.30 angeliefert werden und der Fahrer Granit Beton soll Dir Deine Pizza bringen. Zudem willst Du den Newsletter abonnieren.

\
&nbsp;
\
&nbsp;

### Tabelle zur Testdurchführung
#### Person 1

| Vorgabe?                                                           | Reaktion der testenden Person 1                                                   |
| ------------------------------------------------------------------ | --------------------------------------------------------------------------------- |
| Bestelle eine Pizza mit den Zutaten Salami, Schinken und Mozarella | Testergebnis: OK; Schnelles erkennen, wie das Prinzip der Checkboxen funktioniert |
| Die Lieferung soll um 17.30 geliefert werden auf Pizza             | Testergebnis: OK; Zeit wurde ohne Probleme angepasst                              |
| Wähle ein gewünschtes Getränk aus                                  | Testergebnis: OK; Select Button wurd erkannt                                      |
| Navigiere zur Bestätigungsseite (Eine Seite weiter)                | Testergebnis: OK; Schnelles Erkennen, wie navigiert werden kann                   |
| Wähle Granit Beton als Deinen Fahrer                               | Testergebnis: OK; Fahrer wurde ohne Probleme gewählt.                             |
| Der Newsletter soll abonniert werden/in                            | Testergebnis: OK; Togglebutton wurde erkannt.                                     |
| Die Bestellung wurde abgesendet                                    | Testergebnis: OK;                                                                 |

\
&nbsp;

#### Person 2

| Vorgabe?                                                           | Reaktion der testenden Person 2                                                                     |
| ------------------------------------------------------------------ | --------------------------------------------------------------------------------------------------- |
| Bestelle eine Pizza mit den Zutaten Salami, Schinken und Mozarella | Testergebnis: OK; Schnell erkannt, was wo gewählt werden muss.                                      |
| Die Lieferung soll um 17.30 geliefert werden auf Pizza             | Testergebnis: OK; Eventuell soll der Titel vergrössert werden, damit die Schrift besser lesbar ist. |
| Wähle ein gewünschtes Getränk aus                                  | Testergebnis: OK; Erknnt, dass es eine Select Liste isch.                                           |
| Navigiere zur Bestätigungsseite (Eine Seite weiter)                | Testergebnis: OK; SChnell erkannt, wie zur nächsten Seite navigiert werden kann.                    |
| Wähle Granit Beton als Deinen Fahrer                               | Testergebnis: OK; Die Auswahl wurde schnell getroffen.                                              |
| Der Newsletter soll abonniert werden/in                            | Testergebnis: OK; Toggle Button wurde erkannt und Aufgabe schnell umgesetzt.                        |
| Die Bestellung wurde korrekt abgeschickt                           | Testergebnis: OK;                                                                                   |



\
&nbsp;
\
&nbsp;

# Fazit

Die Testpersonen haben die Tests im grossen und ganzen sehr gut gemeistert.
<br/>Die einzigen zwei Punkte, welche zu Problemen führen könnten, waren die Überschrift bei der Lieferzeitangabe und beim Abbrechen-Button. Die Überschrift wurde von einigen als zu klein empfunden und der Abbrech-Button verleitete dazu, dass er schnell gedrückt werden wollte, anstelle von anderen gewünschten Aktionen.
Wir enschieden uns aber dafür, dass wir die Wireframes/Mokups nicht mehr anpassen werden, da wir das Frontend nicht umsetzten wollen in diesem Modul. Wenn wir aber das Projekt umsetzten würden, dann würden wir auf die Testergebnisse zurückberufen, damit wir sicherstellen können, dass das Fronend Userfreundlich ist.



\
&nbsp;
\
&nbsp;



### Nächstes Kapitel
- [Schlusswort](schlusswort.md)

### Vorheriges Kapitel
- [Schnittstelle](schnittstelle.md)


### Zum Inhaltsverzeichnis wechseln
- [Go to Inhaltsverzeichnis](description.md)