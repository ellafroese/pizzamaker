# Description

Hier ist das Inhaltsverzeichnis, welches helfen soll, dass durch unser Makrdown navigeert werden kann.

## Inhaltsverzeichnis
- [README](README.md)
- [Description](description.md)
- [Kriterien](kriterien.md)
- [Use Cases](usecases.md)
- [Design Frontend](DesignFrontend.md)
- [Schnittstelle](schnittstelle.md)
- [Testing](Testing.md)
- [Schlusswort](schlusswort.md)

## Code
- [Backend Code ohne REST-Schnittstelle](PizzaMaker)
  
  
\
&nbsp;
\
&nbsp;


### Nächstes Kapitel
- [Kriterien](kriterien.md)

### Vorheriges Kapitel
- [Readme](../README.md)