# Use Cases

Im folgenden werden die verschiedenen Use Cases mit mehreren verschiedenen Methoden aufgelistet.


## Vom Text zu den Use Cases


![Vom Text zu den UseCases](Images/TextUseCases.PNG)

## Brainstorming mittels CRC-Karten 

![CRC-Karten1](Images/crc1.PNG)
![CRC-Karten2](Images/crc2.PNG)


## Use Case Ablaufdiagramm

Dieses Diagramm soll ganz simple beschreiben, wie unsere Pizzabestellseite aufgebaut sein wird. Zu erkennen ist, dass wir einen/eine AktorIn haben welche/r auf die Seite zugreift.
Auf der «Page 1» kann der/die UserIn zuerst die Zutaten auswählen, dann das Getränk hinzufügen und gelangt zum Schluss auf die «Page 2». Es besteht die Möglichkeit auf der Ersten Seite den ganzen Bestellprozess zu beenden. <br/>
Auf der zweiten Seite ist eine Bestellübersicht aufgeführt. Hier besteht immer noch die Möglichkeit den ganzen Prozess zu beenden. Auch kann eine Seite zurückgesprungen werden, falls eine Änderung an der Bestellung vorgenommen werden soll. Sobald aber die Bestellung zur Zufriedenheit des Users ist, kann durch einen Absendebutton die Bestellung endgültig abgesendet werden.

![Ablaufdiagramm](Images/UCDiagramm.PNG)


## Use Case Diagramm

![Use Case Diagramm](Images/Diagramm.PNG)

## Use Cases in einer Tabelle


| Use Cases             | Beschreibung                                                                                                                                                                                                                                                                                                                                                                                                                                |
| --------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Zutaten auswählen     | Dem User werden verschiedene Topping Zutaten vorgeschlagen. Diese können mittels Checkbox ausgewählt werden.                                                                                                                                                                                                                                                                                                                                |
| Getränkt auswählen    | Dem User werden verschiedene Getränke vorgeschlagen. Mittels einer Dropdownbox kann ein Getränk ausgewählt werden. Es gibt eine Auswahl, welche «ohne Getränk» heisst. Wenn diese ausgewählt wird, dann wird die Pizza ohne Getränk geliefert.                                                                                                                                                                                              |
| Weitere Einstellungen | In diesem Abschnitt auf der ersten Seite können Einstellungen zu der gewünschten Lieferzeit bearbeitet werden. Auch muss an diesem Punkt einen Kommentar erfasst werden.                                                                                                                                                                                                                                                                    |
| Abbruch               | Auf beiden Seiten gibt es einen Abbruch-Knopf. Sobald dieser gedrückt wird, wird die Bestellung abgebrochen und alles ausgewählten Zutaten werden gelöscht. Diese Funktion kann genutzt werden, wenn sich der/die Kunde/in plötzlich dagegen entscheidet eine Pizza zu bestellen.                                                                                                                                                           |
| Absenden 1            | Die ausgewählten Produkte werden durch das Drücken des Absendebuttons auf der ersten Seite in einer Bestellübersicht aufgeführt.                                                                                                                                                                                                                                                                                                            |
| Bestellübersicht      | In der Bestellübersicht sind alle ausgewählten Zutaten inkl. des Getränkes aufgelistet. Des Weiteren können auf der Bestellübersicht noch weitere Einstellungen vorgenommen werden. An dieser Stelle muss eine E-Mailadresse angegeben werden. Hier kann auch entschieden werden, ob der Newsletter abonniert werden soll oder nicht. Die letzte Auswahl, welche getroffen werden muss, ist welche/r FahrerIn die Lieferung ausfahren soll. |
| Zurück                | Zurück	Mit dieser Funktion kann wieder auf die erste Seite zurückgegriffen werden, falls die Zutaten oder das Getränk bearbeitet werden soll.                                                                                                                                                                                                                                                                                               |
| Absenden              | Mit dem Drücken dieses Buttons wird die Bestellung endgültig abgesendet.                                                                                                                                                                                                                                                                                                                                                                    |

## Fully Dressed Use Cases
![Fully Dressed Use Cases 1](Images/fducNeu1.PNG)	
	

## Klassendiagramm

![Klassendiagramm](Images/klassendiagramm.PNG)	
		
\
&nbsp;
\
&nbsp;
\
&nbsp;	
	
### Nächstes Kapitel 
- [Design Frontend](DesignFrontend.md)

### Vorheriges Kapitel
- [Kriterien](kriterien.md)

### Zum Inhaltsverzeichnis wechseln
- [Go to Inhaltsverzeichnis](description.md)
	

	


