# REST

Representational State Transfer, oder REST, wird insbesondere für Webservices verwendet und regelt die Kommunikation zwischen mehreren Maschinen. REST kann gut im Web verwendet werden, da Systeme wie HTML und XML-Parser vorhanden sind sowie die HTTP-fähigkeit der Clients gegeben ist.

REST ist wegen seiner Skalierbarkeit so interessant. Dadurch, dass REST stateless ist, also zwischen Nachrichten weder der Server noch die Anwendung Informationen speichern muss, macht es das Ganze einfach skalierbar. Auch dadurch, dass das System als Client-Server-Beziehung funktioniert und der Server Dienste zur Verfügung stellt, die ein Client verwenden kann, sorgt dafür, dass der Server unabhängig der Clients skaliert werden kann.

Für REST werden in unserem Beispiel vier Methoden verwendet, welche die meisten durch CRUD kennen. Die CRUD-Methoden «Create», «Read», «Update» und «Delete» werden ersetzt durch «POST», «GET», «PUT» und «DELETE». REST besitzt noch weitere Methoden, welche bei uns nicht verwendet werden.

Die Springboot-API erlaubt uns, durch Get-/Post-/Put- oder Delete-Mapping die Nachrichten zu bearbeiten, lesen oder löschen, und können somit unsere Java-Funktionen entsprechend dem Inhalt der Nachricht ausführen. Das Front-End, welches in unserem Fall nicht programmiert wurde, kann mit dem Programm PostMan simuliert werden. Dabei werden Anfragen erstellt, welche von unserem Backend-Programm verarbeitet werden können.
\
&nbsp;\
&nbsp;


## Nächstes Kapitel
- [Testing](Testing.md)

### Vorheriges Kapitel
- [Design Frontend](DesignFrontend.md)

### Zum Inhaltsverzeichnis wechseln
- [Go to Inhaltsverzeichnis](description.md)