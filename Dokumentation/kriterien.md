# Kriterien

Hier sind alle Kriterien aufgelistet, welche für uns wichtig sind. Dies dient mehr als Übersicht für uns, damit wir alle Kriterien an einem Ort haben. 
\
&nbsp;



## Kriterien für das Backend

| Kriterium                                                     | Status | Wo ist alles zu finden?                                                               |
| ------------------------------------------------------------- | ------ | ------------------------------------------------------------------------------------- |
| klare Beschreibung was die Applikation tun soll               | OK     | Go To [Use Cases](usecases.md)                                                        |
| detailiertes Use Case Diagramm                                | OK     | Go To [Use Cases](usecases.md)                                                        |
| klare Beschreibung von jedem Use Case                         | OK     | Go To [Use Cases](usecases.md)                                                        |
| (evtl. Verknüpfung mit Wireframe / Mock-up GUI)               | OK     | Go To [Design Frontend](DesignFrontend.md)                                            |
| Nachweis Brainstorming mittels CRC-Karten                     | OK     | Go To [Use Cases](usecases.md)                                                        |
| widerspiegelt die Umgebung / Thema                            | OK     | Go To [Use Cases](usecases.md)                                                        |
| zeigt klare Beziehungen auf                                   | OK     | Go To [Use Cases](usecases.md)                                                        |
| zeigt potentielle Methoden & Beziehungen                      | OK     | Go To [Use Cases](usecases.md)                                                        |
| alle Design-Elemente enthalten                                | OK     | Go To [Use Cases](usecases.md)                                                        |
| klare Struktur verfügbar in einem Repository                  | OK     | Go To [README](README.md)                                                             |
| hat eine gewisse Komplexität (Verwendung von einem Framework) | OK     | Go To [Backend Code ohne REST-Schnittstelle](PizzaMaker)                              |
| übersteigt bisherige eigene Projekte (neue Technologien)      | OK     | Decorator pattern verwendet. Go To [Backend Code ohne REST-Schnittstelle](PizzaMaker) |


## Kriterien für das Frontend

Im folgenden sind die Kriterien für das Frontend in Tabellen abgebildet.<br/>
Diese Tabellen wurden vom Repository von Philipp Albrecht kopiert, da diese Tabellen ohne Änderung erwünscht gewesen sind.<br/>
\
&nbsp;
\
&nbsp;



## Kriterium 1 - Versionsverwaltung

| **K1**    |                                                                                                                                                                                                                             |
| --------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Gütestufe | - Versionsverwaltung mit git<br/> - Remoterepository verwendet (github, gitlab, usw.)<br/> - Regelmässige Commits mit sinnvollen Commit-Messages vorhanden<br/> - Keine unnöntigen files eingecheckt (.gitignore verwendet) |
| 3         | Vier Kriterien erfüllt.                                                                                                                                                                                                     |
| 2         | Drei Kriterien erfüllt.                                                                                                                                                                                                     |
| 1         | Eins oder zwei Kriterien erfüllt.                                                                                                                                                                                           |
| 0         | Kein Kriterium erfüllt.                                                                                                                                                                                                     |


## Kriterium 2 - Dokumentationsformat

| **K2**    |                                                                                                                                                                                                                                                                                           |
| --------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Gütestufe | - UseCases (Diagramm, Fully Dressed), Mockups, Klassendiagramme und weitere Dokumente sind sofern möglich in Markdown geschrieben oder als Bilder/Links in Markdown eingebunden.<br/> - Markdown und Bilder mit git versionert.<br/> - Markdown wird auf github o.ä. Korrekt dargestellt. |
| 3         | Drei Kriterien erfüllt.                                                                                                                                                                                                                                                                   |
| 2         | Zwei Kriterien erfüllt.                                                                                                                                                                                                                                                                   |
| 1         | Ein Kriterium erfüllt.                                                                                                                                                                                                                                                                    |
| 0         | Kein Kriterium erfüllt.                                                                                                                                                                                                                                                                   |


## Kriterium 3 - Zusammenfassung und Systemgrenzen                                              

| **K3**    |                                                                                                                                                                                                                                                                                                                                                                                                    |
| --------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Gütestufe | Eine konzeptionelle Zusammenfassung der Arbeit und des erarbeiteten Ergebnisses erleichtert dem mit dem Projekt befassten Leser des Projektes. Grafiken beschreiben die Systemgrenzen und Schnittstellen zur Aussenwelt.                                                                                                                                                                           |
| 3         | 1. Die Kurzfassung richtet sich an die fachlich kompetenten Leser.<br/>2. Die Kurzfassung enthält die Punkte: Kurze Ausgangssituation (Idee) - Umsetzung - Ergebnis. <br/>3. Die Kurzfassung enthält zu jedem dieser genannten Punkte die wesentlichen Aspekte.<br/>4. Die Systemgrenzen und Schnittstellen sind beschrieben.<br/>5. Zur Beschreibungen wurden unterstützende Grafiken eingesetzt. |
| 2         | Vier oder drei Kriterien erfüllt.                                                                                                                                                                                                                                                                                                                                                                  |
| 1         | Mindestens zwei Kriterien erfüllt.                                                                                                                                                                                                                                                                                                                                                                 |
| 0         | Weniger als zwei Kriterien erfüllt.                                                                                                                                                                                                                                                                                                                                                                |


## Kriterium 4 - README.md

| **K4**    |                                                                                                                                                                                                         |
| --------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Gütestufe | 1. Im README.md des Projektes wird die Applikation kurz beschrieben (Kurzfassung).<br/>2. Alle Unterseiten sind verlinkt.<br/>3. Das README hat eine Struktur und hat eine selbsterklärende Navigation. |
| 3         | Drei Kriterien erfüllt.                                                                                                                                                                                 |
| 2         | Zwei Kriterien erfüllt.                                                                                                                                                                                 |
| 1         | Ein Kriterium erfüllt.                                                                                                                                                                                  |
| 0         | Kein Kriterium erfüllt.                                                                                                                                                                                 |



## Kriterium 5 - Funktionsumfang (1)

| **K5**    |                                                                                                                                                                                                                                                  |
| --------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| Gütestufe | - Mind. 2 unterschiedliche Formulare<br/>  - Pro Formular mind. 3 Unterschiedliche Input-Komponenten<br/> - Mind. 6 verschiedene Input-Komponenten verwendet (z.B. Texteingabefeld, Datumeingabefeld, Checkbox, Options, Dropdown, Togglebutton) |
| 3         | Drei Kriterien erfüllt.                                                                                                                                                                                                                          |
| 2         | Zwei Kriterien erfüllt.                                                                                                                                                                                                                          |
| 1         | Ein Kriterium erfüllt.                                                                                                                                                                                                                           |
| 0         | Kein Kriterium erfüllt.                                                                                                                                                                                                                          |


## Kriterium 6 - Funktionsumfang (2)

| **K6**    |                                                                                                                                                        |
| --------- | ------------------------------------------------------------------------------------------------------------------------------------------------------ |
| Gütestufe | - Mind. 1 Dialogfeld (z.B. Bestätigung vor dem Löschen)<br/>- CRUD für mind. 1 Datentyp<br/>- Ein Formular mit Fortschrittsbalken / Stepper umgesetzt. |
| 3         | Drei Kriterien erfüllt.                                                                                                                                |
| 2         | Zwei Kriterien erfüllt.                                                                                                                                |
| 1         | Ein Kriterium erfüllt.                                                                                                                                 |
| 0         | Kein Kriterium erfüllt.                                                                                                                                |


 
## Kriterium 7 - Mockups (oder Wireframes)

| **K7**    |                                                                                                                                               |
| --------- | --------------------------------------------------------------------------------------------------------------------------------------------- |
| Gütestufe |                                                                                                                                               |
| 3         | Für alle Ansichten / Formulare sind Mockups/Wireframes vorhanden. Die Mockups/Wireframes beinhalten alle für den UseCase benötigten Elemente. |
| 2         | Die UseCases sind nur teilweise durch die Mockups/Wireframes abgedeckt.                                                                       |
| 1         | Mockups/Wireframes vorhanden, die meisten fehlen.                                                                                             |
| 0         | Keine Mockups oder Wireframes vorhanden.                                                                                                      |

## Kriterium 8 - Klassendiagramm

| **K8**    |                                                                                                                                           |
| --------- | ----------------------------------------------------------------------------------------------------------------------------------------- |
| Gütestufe |                                                                                                                                           |
| 3         | Es wurde eine gute Klassenstruktur gewählt. Das Klassendiagramm ist vollständig und stimmig. Alle benötigten Beziehungen sind festgelegt. |
| 2         | Das Klassendiagramm hat nur wenige Fehler.                                                                                                |
| 1         | Das Klassendiagramm hat viele Fehler.                                                                                                     |
| 0         | Kein Klassendiagramm vorhanden oder mehrheitlich falsch.                                                                                  |

## Kriterium 9 - Fully dressed UseCases   

| **K9**    |                                                                                                                                                                                                                      |
| --------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Gütestufe | - Jeder UseCase hat einen Namen und eine Nummer<br/>- Jeder UseCase hat einen Primary Actor, Precondition, Main Success Scenario, Postcondition und eventuelle Alternates<br/>- Use Cases decken ganze Anwendung ab. |
| 3         | Drei Kriterien erfüllt.                                                                                                                                                                                              |
| 2         | Zwei Kriterien erfüllt.                                                                                                                                                                                              |
| 1         | Ein Kriterium erfüllt.                                                                                                                                                                                               |
| 0         | Kein Kriterium erfüllt.                                                                                                                                                                                              |


## Kriterium 10 - Ablaufdiagramm

| **K10**   |                                                                                                                                                                                                                                   |
| --------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Gütestufe | - Ablaufdiagramm(e) (z.B. Flussdiagramm oder BPMN) ist/sind vorhanden. <br> - Ablaufdiagramme decken die gesamte Anwendung ab. <br> - Zusammenhänge zwischen den Ablaufdiagrammen und den Mockups/Wireframes sind klar erkennbar. |
| 3         | Alle Kriterien erfüllt.                                                                                                                                                                                                           |
| 2         | Zwei Kriterien erfüllt.                                                                                                                                                                                                           |
| 1         | Ein Kriterium erfüllt.                                                                                                                                                                                                            |
| 0         | Keine Kriterien erfüllt.                                                                                                                                                                                                          |


## Kriterium 11 - Usability Tests 

| **K11**   |                                                                                                                                                                                                                          |
| --------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| Gütestufe | - Ziele, Konzept und Fragestellung für Usability Tests festgelegt und dokumentiert. <br> - Usability Tests mit mehreren Personen durchgeführt und dokumentiert. <br> - Resultate der Tests ausgewertet und dokumentiert. |
| 3         | Alle Kriterien erfüllt.                                                                                                                                                                                                  |
| 2         | Zwei Kriterien erfüllt.                                                                                                                                                                                                  |
| 1         | Ein Kriterium erfüllt.                                                                                                                                                                                                   |
| 0         | Keine Kriterien erfüllt.                                                                                                                                                                                                 |



## Kriterium 12 - REST Schnittstelle 

| **K12**   |                                                                                                                                                                                                             |
| --------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Gütestufe | - Alle für die Applikation benötigten RESTful API requests sind definiert. <br/> - <i>requests parameters</i> und <i>response bodies</i> sind definiert.<br/>- Für jeden API request ein Beispiel erstellt. |
| 3         | Alle Kriterien erfüllt.                                                                                                                                                                                     |
| 2         | Zwei Kriterien erfüllt.                                                                                                                                                                                     |
| 1         | Ein Kriterium erfüllt.                                                                                                                                                                                      |
| 0         | Keine Kriterien erfüllt.                                                                                                                                                                                    |


## Kriterium 13 - Angular Tour of Hereous

| **K13**   |                                                                         |
| --------- | ----------------------------------------------------------------------- |
| Gütestufe | Selbstständiges Durcharbeiten des <i>Tutorial Tour of Heroes. </i>      |
| 3         | Komplett durchgearbeitet. Die Applikation funktioniert.                 |
| 2         | Mehrheitlich durchgearbeitet. Die Applikation funktioniert mehrheitlich |
| 1         | Nur wenige Teile der Applikation funktionieren.                         |
| 0         | Die Applikation funktioniert nicht oder lässt sich nicht kompilieren.   |

## Kriterium 14 - Desktop Applikation

| **K14**   |                                                                         |
| --------- | ----------------------------------------------------------------------- |
| Gütestufe | Übung zu Desktop Applikationen                                          |
| 3         | Komplett durchgearbeitet. Die Applikation funktioniert.                 |
| 2         | Mehrheitlich durchgearbeitet. Die Applikation funktioniert mehrheitlich |
| 1         | Nur wenige Teile der Applikation funktionieren.                         |
| 0         | Die Applikation funktioniert nicht oder lässt sich nicht kompilieren.   |

## Kriterium 15 - Leistungsfähigkeit

| **K15**   |                                                                                                                                                                                                                                                  |
| --------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| Gütestufe | - Der/Die Lernende arbeitete durchwegs engagiert, es gab keine Zeichen von Gleichgültigkeit oder Minimalismus.<br/> - Das Resultat entspricht der Arbeit einer Fachperson.<br/>- Das Potential wurde im Rahmen der vorgegeben Zeit ausgeschöpft. |
| 3         | Drei Kriterien erfüllt.                                                                                                                                                                                                                          |
| 2         | Zwei Kriterien erfüllt.                                                                                                                                                                                                                          |
| 1         | Ein Kriterium erfüllt.                                                                                                                                                                                                                           |
| 0         | Keine Kriterien erfüllt.                                                                                                                                                                                                                         |

## Kriterium 16 - Persönlicher Einsatz und Umsetzung

| **K16**   |                                                                                        |
| --------- | -------------------------------------------------------------------------------------- |
| Gütestufe |                                                                                        |
| 3         | Die Idee und die Umsetzung zeigen einen grossen persönlichen Einsatz und überzeugen.   |
| 2         | Die Idee ist gut. Die Umsetzung erfolgte mehrheitlich gut.                             |
| 1         | Persönlicher Einsatz ist in der Idee und in der Umsetzung nur wenig erkennbar.         |
| 0         | Der persönliche Einsatz ist vorhanden, aber in der Idee und Umsetzung nicht erkennbar. |


\
&nbsp;
\
&nbsp;
Quelle: https://gitlab.com/alptbz/m120/-/blob/master/criteria/README.md




\
&nbsp;
\
&nbsp;
\
&nbsp;

### Nächstes Kapitel
- [Use Cases](usecases.md)

### Vorheriges Kapitel
- [Description](description.md)


### Zum Inhaltsverzeichnis wechseln
- [Go to Inhaltsverzeichnis](description.md)