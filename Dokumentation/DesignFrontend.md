# <bold> Wireframe <bold>

Für das Design des Frontend benutzten wir das Tool Wireframe.cc

\
&nbsp;


## Bestellseite
![Bestellseite](Images/bestellseite.PNG)

\
&nbsp;

## Bestätigungsseite
![Bestätigungsseite](Images/bestaetigungsseite.PNG)	

\
&nbsp;
\
&nbsp;

# Mockup

## Bestellseite
![Übersichtseite](Images/uebersichtseite.PNG)	
\
&nbsp;

## Bestätigungsseite
![Bestellabschluss](Images/bestellabschluss.PNG)	
\
&nbsp;
\
&nbsp;


# Inputkomponenten definieren

| Was?                | Inputkomponente |
| ------------------- | --------------- |
| Kommentar*          | Texteingabefeld |
| Bestellung auf wann | Zeiteingabefeld |
| Zutaten auf Pizza   | Checkbox        |
| Getränk             | Dropdown        |
| Email               | Emaileingabe    |
| Newsletter          | Togglebutton    |
| Fahrer/in           | Options         |


\
&nbsp;

* "*" Das Kommentarfeld haben wir zum Schluss doch weg gelassen.

\
&nbsp;


## Nächstes Kapitel
- [Schnittstelle](schnittstelle.md)

### Vorheriges Kapitel
- [Use Cases](usecases.md)

### Zum Inhaltsverzeichnis wechseln
- [Go to Inhaltsverzeichnis](description.md)