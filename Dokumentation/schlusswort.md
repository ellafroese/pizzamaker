# Schlusswort

In diesem Kapitel werden wir nochmals einen letzten Blick auf unser Projekt werfen.
Das ursprüngliche Ziel des Projektes war, dass wir ein Frontend mit einem Backend Programm verbinden. Hierfür wollten wir eine REST-Schnittstelle verwenden. Doch im Laufe des Projektes haben wir uns dazu entschieden, dass wir kein Frontend Programm schreiben werden. Dennoch konnten wir einen Blick in die Frontend Entwicklung werfen, indem wir eine Angular Tutorial gelöst haben. "Tour of Heros" sollte uns zeigen, wie Angular funktioniert und was die Vorteile daran sind.<br/>
Zu Beginn war uns wichtig, dass wir Use Cases und Diagramme erstellten. Hierbei entstand das Kapitel [Use Cases](usecases.md). Dies beinhaltet alle Diagramme und Tabellen bezüglich des Aufbaus unseres Programms. <br/>
Für die Visualisierung des Frontendes verwendeten wir Mockups und Wireframes. Mit desesn Visualisierungen konnten wir auch unsere Usability Tests durchführen. <br/>
Grosse Schwierigkeiten hatten wir mit den Schnittstellen Programmierung. Hierfür benötigten wir einige Stunden, um zuerst verstehen zu können, wie das Prinzip aufgebaut ist. Danach mussten wir das Ganze noch implementieren, was auch nicht einfach gewesen ist.

\
&nbsp;
\
&nbsp;

### Code
- [Code](../PizzaMaker)

### Vorheriges Kapitel
- [Testing](Testing.md)


### Zum Inhaltsverzeichnis wechseln
- [Go to Inhaltsverzeichnis](description.md)