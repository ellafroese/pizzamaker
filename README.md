# PizzaMaker
In den Modulen 120 und 326 befassen wir uns mit einer Applikation, welche einen Frontend und einen Backend Teil hat. Wir werden als Projekt eine Pizzabestellseite erstellen, welche im Backendteil mit Java arbeitet und im Frontendteil mit REST.

Als Dokumentation für dieses Projekt werden wir in diesem Repository all unsere Erkentnisse festhalten und dokumentieren. 


\
&nbsp;


## Ausgangslage und Ziel

Wir sind in der Lage Programme mit Java zu entwickeln. Das Ziel in diesem Projekt ist es, dass wir eine Java Applikation schreiben werden und zusätzlich ein REST-Schnittstelle verwenden.
Zu Beginn gab es noch das Ziel, dass wir mit Angular einen Frontend-Teil entwickeln wollten. Doch wir entschieden uns dafür, dass wir mit Mokups den Frontendteil darstellen werden, da die Zeit und unser Wissen zu kanpp ist, das Projekt rechtzeitig fertigstellen zu können.

Wie bereits erwähnt dient dieses Repository mit dem Markdown zur Dokumentation. Mit Markdown können wir unser Wissen nochmals enorm erweitern.


\
&nbsp;
\
&nbsp;


# Inhalt des Programmes

Das Programm ist in der Lage eine Pizza und ein Getränk zu bestellen. Es können noch weitere Optionen angewählt werden. Die Lieferzeit kann geändert werden, die Mail Adresse muss angegeben werden, der/die FahrerIn kann gewählt werden und der Newsletter kann abonniert oder deabonniert werden.  
<br/>

Wir arbeiten mit einem Decorator Pattern, damit die Zutaten auf der Pizza hinzugefügt und entfernt werden können. Dieses Pattern ist besser zu warten, als wenn wir für jede Zutat eine eigene Klasse haben und ohne dieses Pattern arbeiten würden. 



### Nächstes Kapitel

- [Description](Dokumentation/description.md)


